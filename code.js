let currentInput = "0";
let lastInput = "";
let operator = null;
let isNewInput = true;

function updateDisplay() {
    document.getElementById("box").innerText = currentInput;
}

function button_number(number) {
    if (isNewInput) {
        currentInput = number.toString();
        isNewInput = false;
    } else {
        currentInput += number.toString();
    }
    updateDisplay();
}

function clear_entry() {
    currentInput = "0";
    isNewInput = true;
    updateDisplay();
}

function button_clear() {
    currentInput = "0";
    lastInput = "";
    operator = null;
    isNewInput = true;
    document.getElementById("last_operation_history").innerText = "";
    updateDisplay();
}

function backspace_remove() {
    currentInput = currentInput.slice(0, -1) || "0";
    updateDisplay();
}

function plus_minus() {
    if (currentInput !== "0") {
        currentInput = currentInput.startsWith("-") ? currentInput.slice(1) : "-" + currentInput;
        updateDisplay();
    }
}

function calculate_percentage() {
    currentInput = (parseFloat(currentInput) / 100).toString();
    updateDisplay();
}

function division_one() {
    currentInput = (1 / parseFloat(currentInput)).toString();
    updateDisplay();
}

function power_of() {
    currentInput = Math.pow(parseFloat(currentInput), 2).toString();
    updateDisplay();
}

function square_root() {
    currentInput = Math.sqrt(parseFloat(currentInput)).toString();
    updateDisplay();
}

function button_operator(op) {
    if (!isNewInput) {
        button_result();
        isNewInput = true
    }
    lastInput = currentInput;
    operator = op;
    isNewInput = true;
    document.getElementById("last_operation_history").innerText = `${lastInput} ${operator}`;
}

function button_result() {
    if (operator === null || isNewInput) return;

    const prev = parseFloat(lastInput);
    const current = parseFloat(currentInput);

    switch (operator) {
        case "+":
            currentInput = (prev + current).toString();
            break;
        case "-":
            currentInput = (prev - current).toString();
            break;
        case "*":
            currentInput = (prev * current).toString();
            break;
        case "/":
            currentInput = current === 0 ? "Error" : (prev / current).toString();
            break;
    }

    operator = null;
    lastInput = "";
    isNewInput = true;
    
    updateDisplay();
}

// Event listeners for operators
document.getElementById("plusOp").onclick = () => button_operator("+");
document.getElementById("subOp").onclick = () => button_operator("-");
document.getElementById("multiOp").onclick = () => button_operator("*");
document.getElementById("divOp").onclick = () => button_operator("/");

// Event listener for equal sign
document.getElementById("equal_sign").onclick = () => button_result();

// Event listeners for number and dot buttons
document.querySelectorAll("#table button").forEach(button => {
    const value = button.innerText;
    if (!isNaN(value) || value === ".") {
        button.onclick = () => button_number(value);
    }
});
